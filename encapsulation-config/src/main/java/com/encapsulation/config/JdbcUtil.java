package com.encapsulation.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcUtil {
    //数据库地址
    private static String URL = "jdbc:mysql://localhost:3306/demo?useSSL=true&useUnicode=true&characterEncoding=UTF-8";
    //取得驱动程序
    private static String DRIVER = "com.mysql.jdbc.Driver";
    //取得用户
    private static String USER = "root";
    //登录密码
    private static String PASSWORD = "123456";

    public JdbcUtil(String url, String driver, String user, String password) {
        this.URL = url;
        this.DRIVER = driver;
        this.USER = user;
        this.PASSWORD = password;
    }

    public JdbcUtil(String url, String user, String password) {
        this.URL = url;
        this.USER = user;
        this.PASSWORD = password;
    }

    //定义获取Connection对象的方法
    public Connection getConnection() throws Exception {
        Class.forName(DRIVER);//将"com.mysql.jdbc.Driver"类的Class类对象加载到运行时内存中
        //定义Connection对象
        Connection conn = null;
        if (conn == null) {
            try {
                conn = DriverManager.getConnection(URL, USER, PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return conn;
    }

    public static void close(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
