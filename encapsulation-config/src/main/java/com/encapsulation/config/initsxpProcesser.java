package com.encapsulation.config;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

@Configuration
public class initsxpProcesser implements EnvironmentPostProcessor, Ordered {

    private static final Integer POST_PROCESSOR_ORDER = Integer.MIN_VALUE + 10;

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        System.out.println("init-out-properties:begin");
        Map<String, Object> map = buildEnv();
        Properties properties = new Properties();
        setParams(properties, map);
        MutablePropertySources propertySources = environment.getPropertySources();
        propertySources.addFirst(new PropertiesPropertySource("application,properties", properties));
        System.out.println("init-out-properties:end");
    }
    //给环境变量赋值
    private void setParams(Properties properties, Map<String, Object> map) {
        if (!CollectionUtils.isEmpty(map)) {
            for (String s : map.keySet()) {
                properties.put(s, map.get(s));
            }
        } else {
            System.out.println("环境配置信息为空，请检查");
        }
    }

    /**
     * 构造需要加入环境变量的配置，真实情况下是访问远程连接获取配置
     */
    private Map<String, Object> buildEnv() {
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            result = QueryDB();
        } catch (Exception e) {
            System.err.println("查询远程数据库失败，请检查");
        }
        return result;
    }
    //获取数据库配置并查询数据
    private Map<String, Object> QueryDB() throws Exception {
        //拿取配置信息
        Properties config = getConfig();
        String url = config.getProperty("jdbc.url");
        String driver = config.getProperty("jdbc.driver");
        String name = config.getProperty("jdbc.name");
        String password = config.getProperty("jdbc.password");
        String mark = config.getProperty("project.mark");
        Connection conn = new JdbcUtil(url, driver, name, password).getConnection();
        //准备sql语句
        String sql = "SELECT * FROM `config`.`common_paramdb` WHERE project_mark = '" + mark + "' and is_value = 1";
        //取得预编译对象
        PreparedStatement pst = conn.prepareStatement(sql);
        //执行sql语句
        ResultSet rs = pst.executeQuery();
        //判断返回值是否存在，并取出返回值
        Map<String, Object> result = new HashMap<String, Object>();
        while (rs.next()){
            result.put(rs.getString("param_key"), rs.getString("param_value"));
        }
        //关闭连接对象
        JdbcUtil.close(conn);
        return result;
    }
    //读取指定路径下的配置
    private Properties getConfig() {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        List<Resource> resourceList = new ArrayList<Resource>();
        getResource(resolver, resourceList, "classpath*:/*.properties");
        String p = "classpath*:/env.properties";
        getResource(resolver, resourceList, p);
        try {
            PropertiesFactoryBean config = new PropertiesFactoryBean();
            config.setLocations(resourceList.toArray(new Resource[]{}));
            config.afterPropertiesSet();
            return config.getObject();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void getResource(PathMatchingResourcePatternResolver resolver, List<Resource> resourceList, String path) {
        try {
            Resource[] resources = resolver.getResources(path);
            for (Resource resource : resources) {
                resourceList.add(resource);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getOrder() {
        return this.POST_PROCESSOR_ORDER + 1;
    }
}
